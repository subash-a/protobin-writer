package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
	"os"
)

func verifySerialization(bytes []byte, obj *Test1) (bool, error) {

	testObj := &Test1{}
	err := proto.Unmarshal(bytes, testObj)
	origStr := obj.GetOneofStringField()
	unmStr := testObj.GetOneofStringField()

	if err == nil {
		if obj.StringField == testObj.StringField && origStr == unmStr {
			return true, nil
		} else {
			return false, nil
		}
	} else {
		return false, err
	}
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	path := os.Args[1]
	if path == "" {
		panic(fmt.Errorf("Usage: go run *.go <directory_name>"))
	}

	testParam1 := &Test1{
		StringField: "TestMessage1",
	}

	testParam2 := &Test1{
		StringField: "TestMessage2",
	}

	testResponse1 := &Test1{
		StringField: "Key1Part1",
		OneofGroup:  &Test1_OneofStringField{"Key1Part2"},
	}

	testResponse2 := &Test1{
		StringField: "Key2Part1",
		OneofGroup:  &Test1_OneofStringField{"Key2Part2"},
	}

	bytesTestParam1, err := proto.Marshal(testParam1)
	checkError(err)
	bytesTestParam2, err := proto.Marshal(testParam2)
	checkError(err)
	bytesTestResponse1, err := proto.Marshal(testResponse1)
	checkError(err)
	bytesTestResponse2, err := proto.Marshal(testResponse2)
	checkError(err)
	// Serialization and deserialization works correctly but the
	// writing of the serialized binary into a file is causing
	// error to be thrown when reading back on Javascript side
	result, serialErr := verifySerialization(bytesTestResponse1, testResponse1)
	checkError(serialErr)
	result, serialErr = verifySerialization(bytesTestResponse2, testResponse2)
	checkError(serialErr)

	if result == true {
		fmt.Println("Serialization is equal")
	} else {
		fmt.Println("Serialization is not equal")
	}

	encTestParam1 := base64.StdEncoding.EncodeToString(bytesTestParam1)
	encTestParam2 := base64.StdEncoding.EncodeToString(bytesTestParam2)
	encTestResponse1 := base64.StdEncoding.EncodeToString(bytesTestResponse1)
	encTestResponse2 := base64.StdEncoding.EncodeToString(bytesTestResponse2)

	myMap := make(map[string]string)
	key1 := "GetTestMessage:" + encTestParam1
	key2 := "GetTestMessage:" + encTestParam2

	myMap[key1] = encTestResponse1
	myMap[key2] = encTestResponse2

	jsonString, marshalErr := json.Marshal(myMap)

	checkError(marshalErr)

	writeErr := ioutil.WriteFile(path+"GetTestMessage.json", jsonString, 0644)

	checkError(writeErr)
}
