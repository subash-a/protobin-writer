#!/bin/bash

set -e
PROTOFILE=$1
if [ -z $PROTOFILE ]
then
	echo "Need to provide a filename"
else
	echo "generating the Go file from $1"
	temp=`mktemp -d /tmp/modelogiq.XXXXXXXX`
	protoc --proto_path="$HOME/Downloads/protoc-3.0.0-beta-2-osx-x86_64" --proto_path="$HOME/Modelogiq/frontend/app/js/protobuf_test" $PROTOFILE --go_out=import_path=main:.
	sed -i .old 's:^import google_protobuf "google/protobuf"$:import google_protobuf "github.com/google/protobuf/descriptor":' ./test.pb.go
	rm ./test.pb.go.old

	protoc --proto_path="$HOME/Downloads/protoc-3.0.0-beta-2-osx-x86_64" --go_out=:$temp $HOME/Downloads/protoc-3.0.0-beta-2-osx-x86_64/google/protobuf/descriptor.proto
	mkdir -p ./vendor/github.com/google/protobuf/descriptor
	mv $temp/google/protobuf/descriptor.pb.go ./vendor/github.com/google/protobuf/descriptor

	rm -rf $temp
fi
